package marcuscf.brick.logic;

import java.awt.Paint;

public class PecaTipo3 extends TipoPeca {
    private final static byte[] shape3a = {
        0, 3, 0, 0,
        3, 3, 3, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
    };
    private final static byte[] shape3b = {
        0, 3, 0, 0,
        3, 3, 0, 0,
        0, 3, 0, 0,
        0, 0, 0, 0,
    };
    private final static byte[] shape3c = {
        0, 0, 0, 0,
        3, 3, 3, 0,
        0, 3, 0, 0,
        0, 0, 0, 0,
    };
    private final static byte[] shape3d = {
        0, 3, 0, 0,
        0, 3, 3, 0,
        0, 3, 0, 0,
        0, 0, 0, 0,
    };

    private final static Paint paintColor = Resources.paintColors[3];

    @Override
    public Paint getPaintColor() {
        return paintColor;
    }

    static byte[][] shapesList = { shape3a, shape3b, shape3c, shape3d };

    @Override
    public byte[][] getShapesList() {
        return shapesList;
    }

}
