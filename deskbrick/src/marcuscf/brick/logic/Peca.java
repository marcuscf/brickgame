package marcuscf.brick.logic;

public class Peca {
    int x;
    int y;
    TipoPeca tipoPeca;

    Peca() {
    }

    public byte[] getShape() {
        if(tipoPeca == null) return null;
        return tipoPeca.getShape();
    }

    public void rotL() {
        tipoPeca.rotL();
    }

    public void rotR() {
        tipoPeca.rotR();
    }

    public void moveL() {
        this.x -= 1;
    }

    public void moveR() {
        this.x += 1;
    }

    public void moveDown() {
        this.y += 1;
    }

    public void moveUp() {
        this.y -= 1;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public TipoPeca getTipoPeca() {
        return tipoPeca;
    }

    public void setTipoPeca(TipoPeca tipoPeca) {
        this.tipoPeca = tipoPeca;
    }

}
