package marcuscf.brick.logic;

import java.awt.Paint;

public class PecaTipo6 extends TipoPeca {
    private static final byte[] shape6a = {
        0, 0, 0, 0,
        0, 6, 6, 0,
        6, 6, 0, 0,
        0, 0, 0, 0,
    };

    private static final byte[] shape6b = {
        6, 0, 0, 0,
        6, 6, 0, 0,
        0, 6, 0, 0,
        0, 0, 0, 0,
    };

    private static final Paint paintColor = Resources.paintColors[6];

    @Override
    public Paint getPaintColor() {
        return paintColor;
    }

    static byte[][] shapesList = { shape6a, shape6b };

    @Override
    public byte[][] getShapesList() {
        return shapesList;
    }
}