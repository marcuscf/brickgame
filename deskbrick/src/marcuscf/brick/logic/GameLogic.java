package marcuscf.brick.logic;

import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class GameLogic {

    private Peca peca = new Peca();

    private final int
            initialPosX = 4,
            fieldHite = 30, // altura (height) em blocos
            fieldWidth = 12; // largura em blocos

    private final byte[][] grid = new byte[fieldHite][fieldWidth];
    private final boolean[] linhaCheia = new boolean[fieldHite];

    public void startGame() {
        clearField();
        selectNewShape(peca);
        peca.setX(initialPosX);
        peca.setY(0);
    }

    private void clearField() {
        for(byte[] row : grid) {
            Arrays.fill(row, (byte) 0);
        }
        Arrays.fill(linhaCheia, false);
    }

    private CollisionType collision(Peca p) {
        byte[] b = p.getShape();
        for(int i = 0, c = 0; i < TipoPeca.shapeSize; ++i) {
            for(int j = 0; j < TipoPeca.shapeSize; ++j, ++c) {
                if(b[c] != 0) {
                    int ii = p.y + i, jj = p.x + j;
                    if(ii >= fieldHite) {
                        return CollisionType.BOTTOM;
                    }
                    if(jj >= fieldWidth || jj < 0) {
                        return CollisionType.SIDE;
                    }
                    if(grid[ii][jj] != 0) {
                        return CollisionType.BOTTOM;
                    }
                }
            }
        }
        return CollisionType.NONE;
    }

    public void clearFullLines() {
        for(int i = 0; i < linhaCheia.length; ++i) {
            if(linhaCheia[i]) {
                for(int j = i - 1; j >= 0; --j) { // da linha anterior até o topo
                    for(int k = 0; k < fieldWidth; ++k) { // copia linha pra baixo
                        grid[j + 1][k] = grid[j][k];
                    }
                }
                linhaCheia[i] = false;
            }
        }
    }

    public void checkLines() {
        for(int i = grid.length - 1; i >= 0; --i) {
            int count = 0;
            for(int j = 0; j < grid[i].length; ++j) {
                if(grid[i][j] != 0) ++count;
            }
            if(count == 0) {
                // linha vazia
                break;
            } else if(count == fieldWidth) {
                // linha cheia
                linhaCheia[i] = true;
            }
        }
    }

    public void moveLeft() {
        peca.moveL();
        if(collision(peca) != CollisionType.NONE) {
            peca.moveR();
        }
    }

    public void moveRight() {
        peca.moveR();
        if(collision(peca) != CollisionType.NONE) {
            peca.moveL();
        }
    }

    public void moveAccelDrop() {
        peca.moveDown();
        if(collision(peca) != CollisionType.NONE) {
            peca.moveUp();
        }
    }

    public void dropOneLine() {
        peca.moveDown();
        if(collision(peca) == CollisionType.BOTTOM) {
            peca.moveUp();
            addToGrid(peca);
            checkLines();
            selectNewShape(peca);
            peca.setX(initialPosX);
            peca.setY(0);
        }
    }

    public void rotateLeft() {
        peca.rotL();
        if(!offScreenCorrection(peca))
            peca.rotR(); // Cancel rotation
    }

    public void rotateRight() {
        peca.rotR();
        if(!offScreenCorrection(peca))
            peca.rotL(); // Cancel rotation
    }

    /* Returns false if the rotation is not possible.
    Returns true if rotation is possible, with or without correction. */
    private boolean offScreenCorrection(Peca p) {
        byte[] b = p.getShape();
        for(int i = 0, c = 0; i < TipoPeca.shapeSize; ++i) {
            for(int j = 0; j < TipoPeca.shapeSize; ++j, ++c) {
                if(b[c] != 0) {
                    int ii = p.y + i;

                    if(ii >= fieldHite) return false;

                    int jj = p.x + j;

                    if(jj >= fieldWidth) {
                        p.x -= 1;
                        return offScreenCorrection(p); // goto inicio
                    }

                    if(jj < 0) {
                        p.x += 1;
                        return offScreenCorrection(p); // goto inicio
                    }

                    if(grid[ii][jj] != 0) return false;
                }
            }
        }
        return true;
    }

    private void addToGrid(Peca p) {
        byte[] b = p.getShape();
        for(int i = 0, c = 0; i < TipoPeca.shapeSize; ++i) {
            for(int j = 0; j < TipoPeca.shapeSize; ++j, ++c) {
                if(b[c] != 0)
                    grid[p.y + i][p.x + j] = b[c];
            }
        }
    }

    private final Random random = ThreadLocalRandom.current();
    private int currentShapeIndex = Integer.MAX_VALUE;

    private void selectNewShape(Peca peca) {
        if(currentShapeIndex >= pecasDisponiveis.length) {
            Collections.shuffle(Arrays.asList(pecasDisponiveis), random);
            currentShapeIndex = 0;
        }
        peca.setTipoPeca(pecasDisponiveis[currentShapeIndex]);
        ++currentShapeIndex;
    }

    // Funcionam como singletons. Este array será reordenado para randomizar a ordem das peças
    TipoPeca[] pecasDisponiveis = {
        new PecaTipo1(),
        new PecaTipo2(),
        new PecaTipo3(),
        new PecaTipo4(),
        new PecaTipo5(),
        new PecaTipo6(),
        new PecaTipo7(),
    };

    public int getFieldHite() {
        return fieldHite;
    }

    public int getFieldWidth() {
        return fieldWidth;
    }

    public byte[][] getGrid() {
        return grid;
    }

    public Peca getPeca() {
        return peca;
    }

    public boolean[] getLinhaCheia() {
        return linhaCheia;
    }
}
