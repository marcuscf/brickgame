package marcuscf.brick.logic;

import java.awt.Paint;

public class PecaTipo5 extends TipoPeca {
    private final static byte[] shape5a = {
        0, 0, 0, 0,
        5, 5, 5, 0,
        0, 0, 5, 0,
        0, 0, 0, 0,
    };

    private final static byte[] shape5b = {
        0, 5, 5, 0,
        0, 5, 0, 0,
        0, 5, 0, 0,
        0, 0, 0, 0,
    };

    private final static byte[] shape5c = {
        5, 0, 0, 0,
        5, 5, 5, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
    };

    private final static byte[] shape5d = {
        0, 5, 0, 0,
        0, 5, 0, 0,
        5, 5, 0, 0,
        0, 0, 0, 0,
    };

    private static final Paint paintColor = Resources.paintColors[5];

    @Override
    public Paint getPaintColor() {
        return paintColor;
    }

    static byte[][] shapesList = { shape5a, shape5b, shape5c, shape5d };

    @Override
    public byte[][] getShapesList() {
        return shapesList;
    }
}