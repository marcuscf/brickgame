package marcuscf.brick.logic;

import java.awt.Paint;

// TODO: Que tal usar enum?
public abstract class TipoPeca {

    public static final int shapeSize = 4;

    public abstract byte[][] getShapesList();

    public abstract Paint getPaintColor();

    int currentShape = 0;

    void setInitialShape() {
        currentShape = 0;
    }

    byte[] getShape() {
        return getShapesList()[currentShape];
    }

    void rotL() {
        currentShape = (currentShape + 1) % getShapesList().length;
    }

    void rotR() {
        currentShape -= 1;
        if (currentShape < 0) {
            currentShape = getShapesList().length - 1;
        }
    }

}
