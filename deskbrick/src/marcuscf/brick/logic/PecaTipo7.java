package marcuscf.brick.logic;

import java.awt.Paint;

public class PecaTipo7 extends TipoPeca {
    private final static byte[] shape7a = {
        0, 0, 0, 0,
        7, 7, 0, 0,
        0, 7, 7, 0,
        0, 0, 0, 0,
    };

    private final static byte[] shape7b = {
        0, 7, 0, 0,
        7, 7, 0, 0,
        7, 0, 0, 0,
        0, 0, 0, 0,
    };

    private static final Paint paintColor = Resources.paintColors[7];

    @Override
    public Paint getPaintColor() {
        return paintColor;
    }

    static byte[][] shapesList = { shape7a, shape7b };

    @Override
    public byte[][] getShapesList() {
        return shapesList;
    }
}