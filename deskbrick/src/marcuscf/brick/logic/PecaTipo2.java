package marcuscf.brick.logic;

import java.awt.Paint;

public class PecaTipo2 extends TipoPeca {
    private final static byte[] shape2a = {
        0, 0, 0, 0,
        0, 0, 0, 0,
        2, 2, 2, 2,
        0, 0, 0, 0,
    };
    private final static byte[] shape2b = {
        0, 2, 0, 0,
        0, 2, 0, 0,
        0, 2, 0, 0,
        0, 2, 0, 0,
    };

    private final static Paint paintColor = Resources.paintColors[2];

    @Override
    public Paint getPaintColor() {
        return paintColor;
    }

    static byte[][] shapesList = { shape2a, shape2b };

    @Override
    public byte[][] getShapesList() {
        return shapesList;
    }

}
