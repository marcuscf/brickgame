package marcuscf.brick.logic;

public enum CollisionType {
    NONE, BOTTOM, SIDE
}
