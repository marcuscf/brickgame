package marcuscf.brick.logic;

/**
 *
 * @author marcuscf
 */
public enum GameState {
    NOT_STARTED, STARTED, PAUSED
}
