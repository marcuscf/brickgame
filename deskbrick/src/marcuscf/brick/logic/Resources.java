package marcuscf.brick.logic;

import java.awt.Color;
import java.awt.Paint;

public class Resources {
    public static Paint[] paintColors = {
        Color.BLACK, // cor das bordas
        new Color(130, 16, 0), // cor da peça 1
        new Color(3, 50, 89),
        new Color(0, 0, 255),
        new Color(0, 127, 0),
        new Color(255, 0, 0),
        new Color(0, 128, 255),
        new Color(255, 127, 0), // cor da peça 7
    };

}
