package marcuscf.brick.logic;

import java.awt.Paint;

public class PecaTipo4 extends TipoPeca {
    private final static byte[] shape4a = {
        0, 0, 4, 0,
        4, 4, 4, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
    };

    private final static byte[] shape4b = {
        4, 4, 0, 0,
        0, 4, 0, 0,
        0, 4, 0, 0,
        0, 0, 0, 0,
    };

    private final static byte[] shape4c = {
        0, 0, 0, 0,
        4, 4, 4, 0,
        4, 0, 0, 0,
        0, 0, 0, 0,
    };

    private final static byte[] shape4d = {
        0, 4, 0, 0,
        0, 4, 0, 0,
        0, 4, 4, 0,
        0, 0, 0, 0,
    };

    private final static Paint paintColor = Resources.paintColors[4];

    @Override
    public Paint getPaintColor() {
        return paintColor;
    }

    static byte[][] shapesList = { shape4a, shape4b, shape4c, shape4d };

    @Override
    public byte[][] getShapesList() {
        return shapesList;
    }
}