package marcuscf.brick.logic;

import java.awt.Paint;

public class PecaTipo1 extends TipoPeca {
    private final static byte[] shape1 = {
        0, 0, 0, 0,
        0, 1, 1, 0,
        0, 1, 1, 0,
        0, 0, 0, 0,
    };

    private final static Paint paintColor = Resources.paintColors[1];

    @Override
    public Paint getPaintColor() {
        return paintColor;
    }

    private final static byte[][] shapesList = { shape1 };

    @Override
    public byte[][] getShapesList() {
        return shapesList;
    }

}
