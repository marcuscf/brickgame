package marcuscf.brick.desktop;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;

import java.awt.event.ActionEvent;

import javax.swing.BorderFactory;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;

// É um applet mas funciona pelo main() também.
/**
 * 
 * @deprecated See GameFrame
 * @see GameFrame
 */
@Deprecated
public class GameMain extends JApplet {
    private PlayingField playingField = new PlayingField();
    private JPanel controlPanel = new JPanel();
    private JButton btnStart = new JButton();
    private JButton btnPause = new JButton();

    public GameMain() {
    }

    private void initPositions() throws Exception {
        this.getContentPane().setLayout(new BorderLayout());
        this.setSize(new Dimension(449, 476));
        playingField.setMinimumSize(new Dimension(150, 300));
        playingField.setSize(new Dimension(205, 476));
        playingField.setPreferredSize(new Dimension(205, 410));
        controlPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        controlPanel.setBackground(new Color(137, 221, 140));
        controlPanel.setLayout(null);
        controlPanel.setPreferredSize(new Dimension(150, 100));
        controlPanel.setMinimumSize(new Dimension(150, 100));
        btnStart.setText("Start");
        btnStart.setBounds(new Rectangle(40, 25, 90, 23));
        btnStart.setActionCommand("Start");
        btnStart.addActionListener(this::btnStart_actionPerformed);
        btnPause.setText("Pause");
        btnPause.setBounds(new Rectangle(40, 85, 90, 23));
        btnPause.setActionCommand("Pause");
        btnPause.addActionListener(this::btnPause_actionPerformed);
        this.getContentPane().add(playingField, BorderLayout.LINE_START);
        controlPanel.add(btnPause, null);
        controlPanel.add(btnStart, null);
        this.getContentPane().add(controlPanel, BorderLayout.LINE_END);
    }

    @Override
    public void init() {
        try {
            initPositions();
            DesktopInput.initInputMap(playingField);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        GameMain gameMain = new GameMain();
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(gameMain, BorderLayout.CENTER);
        frame.setTitle("Game");
        gameMain.init();
        gameMain.start();
        frame.setSize(362, 550);
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = frame.getSize();
        //frame.setLocation((d.width-frameSize.width)/2, (d.height-frameSize.height)/2);
        frame.setVisible(true);
    }

    private void btnStart_actionPerformed(ActionEvent e) {
        // Adicionar pergunta confirmando se deseja cancelar o jogo corrente.
        playingField.startGame();
    }

    private void btnPause_actionPerformed(ActionEvent e) {
        playingField.togglePause();
    }
}
