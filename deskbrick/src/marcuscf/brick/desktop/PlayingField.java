package marcuscf.brick.desktop;

import marcuscf.brick.logic.TipoPeca;
import marcuscf.brick.logic.Peca;
import java.awt.Color;
import java.awt.Graphics;

import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;
import marcuscf.brick.logic.GameLogic;
import marcuscf.brick.logic.GameState;
import marcuscf.brick.logic.Moving;
import marcuscf.brick.logic.Resources;

// TODO:
// - Melhorar controles (às vezes anda dois passos por vez)
// - Mais funcionalidades (restart, help, etc.)
// - Atualizar infraestrutura (tirar referências a applets, talvez portar para Android)

public final class PlayingField extends JPanel implements ActionListener {

    private final int
        updateInterval = 100, // intervalo entre as chamadas de updateGame em milissegundos
        dropCount = 4, // quantidade de vezes que tem que passar pelo updateGame para a peça cair uma linha
        blinkCount = 2;
    private int
        drop = 0,
        blink = 0;

    //private long moveStartTimestamp = 0;
    private Moving moving = Moving.NONE;

    private final Timer timer = new Timer(updateInterval, this);
    private GameState gameState = GameState.NOT_STARTED;

    private final GameLogic gameLogic = new GameLogic();

    public PlayingField() {
        this.setLayout(null);
        this.setBackground(new Color(255, 224, 202));
    }

    public void startGame() {
        gameLogic.startGame();
        unpauseGame();
    }

    public void pauseGame() {
        timer.stop();
        gameState = GameState.PAUSED;
    }

    public void unpauseGame() {
        timer.start();
        gameState = GameState.STARTED;
    }

    public void togglePause() {
        switch(gameState) {
            case STARTED:
                pauseGame();
                break;
            case PAUSED:
                unpauseGame();
                break;
            case NOT_STARTED: // do nothing
                break;
        }
    }

    public boolean isPaused() {
        return gameState == GameState.PAUSED;
    }
    
    public int blocksToPixels(int numOfBlocks) {
        int pw = getWidth() / gameLogic.getFieldWidth();
        int ph = getHeight() / gameLogic.getFieldHite();
        int blockSize = pw < ph ? pw : ph;        
        return blockSize * numOfBlocks;
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawPiece(gameLogic.getPeca(), (Graphics2D) g);
        drawGrid((Graphics2D) g);
    }

    private void drawPiece(Peca p, Graphics2D g) {
        if(p == null || p.getShape() == null) return;

        byte[] b = p.getShape();
        g.setPaint(p.getTipoPeca().getPaintColor());
        for(int i = 0, c = 0; i < TipoPeca.shapeSize; ++i) {
            for(int j = 0; j < TipoPeca.shapeSize; ++j, ++c) {
                if(b[c] != 0) {
                    g.drawRect(blocksToPixels(p.getX() + j), blocksToPixels(p.getY() + i), blocksToPixels(1), blocksToPixels(1));
                }
            }
        }
    }

    private void drawGrid(Graphics2D g) {
        int sz = blocksToPixels(1);
        for(int i = 0; i < gameLogic.getFieldHite(); ++i) {
            if(gameLogic.getLinhaCheia()[i]) {
                g.setPaint(Color.CYAN);
                g.fillRect(0, blocksToPixels(i),
                    blocksToPixels(gameLogic.getFieldWidth()), sz);
            } else {
                for(int j = 0; j < gameLogic.getFieldWidth(); ++j) {
                    if(gameLogic.getGrid()[i][j] != 0) {
                        int posI = blocksToPixels(i);
                        int posJ = blocksToPixels(j);
                        g.setPaint(Resources.paintColors[gameLogic.getGrid()[i][j]]);
                        g.fillRect(posJ, posI, sz, sz);
                        g.setPaint(Resources.paintColors[0]);
                        g.drawRect(posJ, posI, sz, sz);
                    }
                }
            }
        }
    }

    private void updateGame() {
        blink += 1;
        if(blink >= blinkCount) {
            blink = 0;
            gameLogic.clearFullLines();
        }

        // Rascunhos para melhorar o tratamento do teclado...
        //
        //long currentTimestamp = System.currentTimeMillis();
        //long dif = currentTimestamp - moveStartTimestamp;
        //System.out.println("start: " + moveStartTimestamp + " end: " + currentTimestamp + " difference: " + dif);
        //if(dif > updateInterval && dif < 2 * updateInterval) {
        // // ignorar tecla (?)
        //}

        switch(moving) {
            case LEFT:
                gameLogic.moveLeft();
                break;

            case RIGHT:
                gameLogic.moveRight();
                break;

            case DOWN:
                gameLogic.moveAccelDrop();
                break;
        }
        moving = Moving.NONE; // usar repetição normal de eventos do teclado. Não é o ideal...

        drop += 1;
        if(drop >= dropCount) {
            drop = 0;
            gameLogic.dropOneLine();
        }
    }

    // Timer event
    @Override
    public void actionPerformed(ActionEvent e) {
        updateGame();
        repaint();
    }

    public void startMovingLeft() {
        if(moving != Moving.LEFT) {
            //moveStartTimestamp = System.currentTimeMillis();
            moving = Moving.LEFT;
        }
    }

    public void startMovingRight() {
        if(moving != Moving.RIGHT) {
            //moveStartTimestamp = System.currentTimeMillis();
            moving = Moving.RIGHT;
        }
    }

    void startMovingDown() {
        if(moving != Moving.DOWN) {
            //moveStartTimestamp = System.currentTimeMillis();
            moving = Moving.DOWN;
        }
    }

    public void stopMovingLeft() {
        if(moving == Moving.LEFT)
            moving = Moving.NONE;
    }

    public void stopMovingRight() {
        if(moving == Moving.RIGHT)
            moving = Moving.NONE;
    }

    public void stopMovingDown() {
        if(moving == Moving.DOWN)
            moving = Moving.NONE;
    }

    public void rotateLeft() {
        gameLogic.rotateLeft();
    }

    public void rotateRight() {
        gameLogic.rotateRight();
    }
}
