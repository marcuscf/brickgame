package marcuscf.brick.desktop;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.KeyStroke;

public class DesktopInput {

    public static void initInputMap(PlayingField playingField) {
        InputMap inMap = playingField.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
        inMap.put(KeyStroke.getKeyStroke("A"), "startMovingLeft");
        inMap.put(KeyStroke.getKeyStroke("LEFT"), "startMovingLeft");
        inMap.put(KeyStroke.getKeyStroke("DELETE"), "startMovingLeft");

        inMap.put(KeyStroke.getKeyStroke("S"), "startMovingDown");
        inMap.put(KeyStroke.getKeyStroke("DOWN"), "startMovingDown");
        inMap.put(KeyStroke.getKeyStroke("END"), "startMovingDown");

        inMap.put(KeyStroke.getKeyStroke("D"), "startMovingRight");
        inMap.put(KeyStroke.getKeyStroke("RIGHT"), "startMovingRight");
        inMap.put(KeyStroke.getKeyStroke("PAGE_DOWN"), "startMovingRight");

        inMap.put(KeyStroke.getKeyStroke("G"), "rotateLeft");
        inMap.put(KeyStroke.getKeyStroke("W"), "rotateLeft");
        inMap.put(KeyStroke.getKeyStroke("UP"), "rotateLeft");
        inMap.put(KeyStroke.getKeyStroke("HOME"), "rotateLeft");

        inMap.put(KeyStroke.getKeyStroke("H"), "rotateRight");
        inMap.put(KeyStroke.getKeyStroke("E"), "rotateRight");
        inMap.put(KeyStroke.getKeyStroke("PAGE_UP"), "rotateRight");

        inMap.put(KeyStroke.getKeyStroke("released A"), "stopMovingLeft");
        inMap.put(KeyStroke.getKeyStroke("released LEFT"), "stopMovingLeft");
        inMap.put(KeyStroke.getKeyStroke("released DELETE"), "stopMovingLeft");

        inMap.put(KeyStroke.getKeyStroke("released S"), "stopMovingDown");
        inMap.put(KeyStroke.getKeyStroke("released DOWN"), "stopMovingDown");
        inMap.put(KeyStroke.getKeyStroke("released END"), "stopMovingDown");

        inMap.put(KeyStroke.getKeyStroke("released D"), "stopMovingRight");
        inMap.put(KeyStroke.getKeyStroke("released RIGHT"), "stopMovingRight");
        inMap.put(KeyStroke.getKeyStroke("released PAGE_DOWN"), "stopMovingRight");

        inMap.put(KeyStroke.getKeyStroke("PAUSE"), "togglePause");

        //inMap.put(KeyStroke.getKeyStroke("M"), "debugKey");
        ActionMap actMap = playingField.getActionMap();

        //actMap.put("debugKey", new AbstractAction() {
        //            public void actionPerformed(ActionEvent e) {
        //                Dimension fieldSize = playingField.getSize();
        //                System.out.println("playingField: " + fieldSize);
        //            }
        //        });
        actMap.put("startMovingLeft", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playingField.startMovingLeft();
            }
        });
        actMap.put("startMovingRight", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playingField.startMovingRight();
            }
        });
        actMap.put("startMovingDown", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playingField.startMovingDown();
            }
        });
        actMap.put("stopMovingLeft", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playingField.stopMovingLeft();
            }
        });
        actMap.put("stopMovingRight", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playingField.stopMovingRight();
            }
        });
        actMap.put("stopMovingDown", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playingField.stopMovingDown();
            }
        });
        actMap.put("rotateLeft", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playingField.rotateLeft();
            }
        });
        actMap.put("rotateRight", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playingField.rotateRight();
            }
        });
        actMap.put("togglePause", new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                playingField.togglePause();
            }
        });
    }
}
